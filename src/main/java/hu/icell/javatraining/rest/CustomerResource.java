package hu.icell.javatraining.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import hu.icell.javatraining.model.Customer;

@Path(value = "customer")
public class CustomerResource {

	private List<Customer> customers = new ArrayList<Customer>();

	{
		Customer c = new Customer();
		c.setAge(31);
		c.setAddress("Ulloi ut");
		c.setFirstName("Attila");
		c.setLastName("Balogh");
		c.setSecureCode("23232323A");
		c.setCustomerId(1);
		customers.add(c);
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getCustomers() {
		GenericEntity<List<Customer>> entity = new GenericEntity<List<Customer>>(customers) {
		};
		return Response.status(Response.Status.OK).entity(entity).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomersAsJSON() {
		GenericEntity<List<Customer>> entity = new GenericEntity<List<Customer>>(customers) {
		};
		return Response.status(Response.Status.OK).entity(entity).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getUserById(@PathParam("id") Integer id) {

		for (Customer c : customers) {
			if (c.getCustomerId() == id) {
				return Response.status(Response.Status.OK).entity(c).build();
			}
		}

		return Response.status(Status.NOT_FOUND).header("message", "Customer with the specific id not found: " + id)
				.build();

	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserByIdAsJson(@PathParam("id") Integer id) {

		for (Customer c : customers) {
			if (c.getCustomerId() == id) {
				return Response.status(Response.Status.OK).entity(c).build();
			}
		}

		return Response.status(Status.NOT_FOUND).header("message", "Customer with the specific id not found: " + id)
				.build();

	}

}
