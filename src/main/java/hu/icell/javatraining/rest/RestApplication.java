package hu.icell.javatraining.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value = "/api")
public class RestApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {

		Set<Class<?>> clazzes = new HashSet<Class<?>>();
		clazzes.add(CustomerResource.class);
		return clazzes;
	}

}
